<?php

use Illuminate\Support\Facades\Route;
use Modules\Order\Controllers\OrderRestController;

Route::prefix('api')->group(function () {
    Route::resource('orders', OrderRestController::class);
});
