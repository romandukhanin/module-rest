<?php

namespace Modules\Order\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderShowResource extends JsonResource
{

    public static $wrap = 'order';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'sum' => $this->sum,
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'ok',
        ];
    }
}
