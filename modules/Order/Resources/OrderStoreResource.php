<?php

namespace Modules\Order\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderStoreResource extends JsonResource
{

    public static $wrap = 'store';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid' => $this->uid,
            'name' => $this->name,
            'sum' => $this->sum,
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'store',
        ];
    }
}
