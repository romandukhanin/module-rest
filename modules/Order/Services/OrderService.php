<?php

namespace Modules\Order\Services;

use Modules\Order\Models\Order;

class OrderService
{

    public function store(array $data): Order
    {
        $data['created'] = now();

        return Order::create($data);
    }

    public function update(array $data, int $id): ?Order
    {

        $order = Order::find($id)->fill($data);

        return $order->save() ? $order : null;
    }
}
