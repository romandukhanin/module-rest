<?php

namespace Modules\Order\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Order\Models\Order;
use Modules\Order\Resources\OrderShowResource;
use Modules\Order\Resources\OrderStoreResource;
use Modules\Order\Resources\OrderUpdateResource;
use Modules\Order\Requests\StoreOrderRequest;
use Modules\Order\Requests\UpdateOrderRequest;
use Modules\Order\Services\OrderService;

class OrderRestController extends Controller
{

    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function store(StoreOrderRequest $request)
    {
        $validated = $request->validated();

        $order = $this->orderService->store($validated);

        return new OrderStoreResource($order);
    }

    public function update(UpdateOrderRequest $request, $id)
    {
        return new OrderUpdateResource($this->orderService->update($request->validated(), (int)$id));
    }

    public function show($id)
    {
        $order = Order::find($id);
        return new OrderShowResource($order);
    }
}
